import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, Observer} from "rxjs";

export interface Task {
  id?: number,
  created_at?: Date,
  name: string,
  details?: string,
  priority_id?: number
  priority_name?: string
}

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private _tasksObservable: Observable<Task[]>;
  private _tasksObserver: Observer<Task[]>;

  constructor(private http: HttpClient) {
    this._tasksObservable = new Observable(observer => {
      this._tasksObserver = observer;
      return {
        unsubscribe(): void {
        }
      };
    });
    this._tasksObservable.subscribe().unsubscribe();
  }

  getTasks(): Observable<Task[]> {
    this.http.get("/tasks")
      .subscribe(
        (tasks: Task[]) => {
          this._tasksObserver.next(tasks);
        },
        err => {
          this._tasksObserver.error(err);
        }
      );
    return this._tasksObservable;
  }

  saveTask(task: Task): Promise<Task> {
    return new Promise<Task>(((resolve, reject) => {
      const taskDB = {
        id: task.id || null,
        name: task.name || null,
        details: task.details || null,
        priority_id: task.priority_id || null
      };

      this.http.post("/tasks", taskDB)
        .subscribe(
          (task: Task) => {
            resolve(task);
          },
          err => {
            reject(err.error);
          }
        )
    }));
  }

  deleteTask(taskId: number): Promise<void> {
    return new Promise<void>(((resolve, reject) => {
      this.http.delete('/tasks/' + taskId)
        .subscribe(
          () => resolve(),
          err => reject(err.error)
        );
    }));
  }
}
