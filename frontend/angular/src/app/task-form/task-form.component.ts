import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {TaskPriority, TaskPriorityService} from "../task-priority.service";
import {TaskService, Task} from "../task.service";

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.scss']
})
export class TaskFormComponent implements OnInit {

  taskPriorities: TaskPriority[];

  taskForm = <FormGroup>new FormGroup({
    id: new FormControl({value: '', disabled: true}),
    name: new FormControl(),
    details: new FormControl(),
    priorityId: new FormControl()
  });

  constructor(taskPriorityService: TaskPriorityService, private taskService: TaskService) {
    taskPriorityService.getPriorities()
      .then((priorities: TaskPriority[]) => {
        priorities.unshift(<TaskPriority>{id: null, name: ''});
        this.taskPriorities = priorities;
      })
      .catch(err => {
        console.error("Failed to get task priorities: ", err);
      });
  }

  ngOnInit() {
  }

  submitTaskForm() {
    const task = <Task>{
      name: this.taskForm.controls.name.value,
      details: this.taskForm.controls.details.value,
      priority_id: this.taskForm.controls.priorityId.value
    };
    this.taskService.saveTask(task)
      .then(() => {
        this.taskForm.reset();
        this.taskService.getTasks();
      })
      .catch(err => {
        console.error(err);
        alert(err.error);
      });
  }

}
