import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor} from '@angular/common/http';
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class APIInterceptorService implements HttpInterceptor {

  apiUrl = 'http://localhost:3000/api';

  constructor() {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const update = <any>{
      setHeaders: {
        "Content-Type": "application/json"
      }
    };

    if (!req.url.match(/^http/)) {
      update.url = this.apiUrl + req.url;
    }

    const request = req.clone(update);

    return next.handle(request);
  }
}
