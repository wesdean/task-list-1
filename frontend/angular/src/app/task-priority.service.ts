import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

export interface TaskPriority {
  id: number
  name: string
  sort: number
}

@Injectable({
  providedIn: 'root'
})
export class TaskPriorityService {

  constructor(private http: HttpClient) {
  }

  getPriorities(): Promise<TaskPriority[]> {
    return new Promise<TaskPriority[]>(((resolve, reject) => {
      this.http.get('/priorities')
        .subscribe(
          (priorities: TaskPriority[]) => {
            resolve(priorities);
          },
          (err) => {
            reject(err);
          }
        );
    }));
  }
}
