import {Component, OnInit} from '@angular/core';
import {Task, TaskService} from "../task.service";

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {

  tasks: Task[];

  constructor(private taskService: TaskService) {
    this.taskService.getTasks()
      .subscribe(
        tasks => {
          this.tasks = tasks;
        }
      )
  }

  ngOnInit() {
  }

  taskCount(): number {
    if (this.tasks && Array.isArray(this.tasks)) {
      return this.tasks.length;
    }
    return 0;
  }

  deleteTask(taskId: number): void {
    this.taskService.deleteTask(taskId)
      .then(() => this.taskService.getTasks())
      .catch(err => {
        console.log(err);
        alert(err.error);
      })
  }

}
