import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TasksListPageComponent} from "./tasks-list-page/tasks-list-page.component";

const routes: Routes = [
  {path: '', redirectTo: '/tasks', pathMatch: 'full'},
  {path: 'tasks', component: TasksListPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
