ANGULAR

Create a Task List.

Step 1:
Using Angular create a form where a user can enter a task name and click a save button.
Once the button is clicked the textfield should clear and the task should appear in a list below the form.
Empty tasks are not allowed.
Add a link to each task to allow you to remove the item.
A dropdown with priority to the form with the values:
* High
* Medium
* Low

ex:

___________ [Priority v ] [Save]

- Task1 (High) [remove]
- Task2 (Medium) [remove]
- Task3 (Low) [remove]
- Task4 (Low) [remove]

Step 2:
Using Nodejs create a REST API to manage the tasks.
The API should have methods to retrieve all tasks, remove a task, and list the available priority values.
Integrate your Angular app to use it.