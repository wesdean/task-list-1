CREATE TABLE [dbo].[tasks]
(
  [id]          [bigint]                                            NOT NULL IDENTITY (1, 1),
  [created_at]  [datetime]                                          NULL DEFAULT (getdate()),
  [name]        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [details]     [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
  [priority_id] [bigint]                                            NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tasks]
  ADD CONSTRAINT [PK_tasks] PRIMARY KEY CLUSTERED ([id]) ON [PRIMARY]
GO

GRANT INSERT, DELETE ON OBJECT::tasklist.dbo.tasks TO [tasklist-api]
GO