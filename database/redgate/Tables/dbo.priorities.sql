CREATE TABLE [dbo].[priorities]
(
  [id]   [bigint]      NOT NULL IDENTITY (1, 1),
  [name] [varchar](10) NOT NULL,
  [sort] [int]         NOT NULL DEFAULT 1
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[priorities]
  ADD CONSTRAINT [PK_priorities] PRIMARY KEY CLUSTERED ([id]) ON [PRIMARY]
ALTER TABLE [dbo].[priorities]
  ADD CONSTRAINT [UK_priority_name] UNIQUE ([name])
GO