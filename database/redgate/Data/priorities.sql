INSERT INTO [dbo].[priorities] ([name], [sort])
VALUES ('High', 1);

INSERT INTO [dbo].[priorities] ([name], [sort])
VALUES ('Medium', 2);

INSERT INTO [dbo].[priorities] ([name], [sort])
VALUES ('Low', 3);