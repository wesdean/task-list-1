const defaults = {
  port: 3000,
  host: "0.0.0.0",
};

const app = {
  name: "task-list-1",
  baseUrl: `${defaults.host}:${defaults.port}`,
  protocol: "http",
  port: defaults.port,
  hostname: defaults.host,
};

const logs = {
  appenders: {
    console: {
      type: "console",
      layout: {
        type: "pattern",
        pattern: "[%d] [%host] [%z] [%X{name}] [%X{identifier}] [%p] - %m%n",
      },
    },
    error: {
      type: "file",
      filename: "/var/log/task-list-1/error.log",
    },
    debug: {
      type: "file",
      filename: "/var/log/task-list-1/debug.log",
    },
  },
  categories: {
    default: {
      appenders: ["console"],
      level: "debug",
    },
    error: {
      appenders: ["error"],
      level: "error",
    },
    debug: {
      appenders: ["debug"],
      level: "debug",
    },
  }
};

const database = {
  databaseName: process.env.DATABASE_NAME || "tasklist",
  hostname: process.env.DB_SERVER,
  port: process.env.DB_PORT || 1433,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  dialect: process.env.DB_DIALECT || "mssql",
  encrypt: !!process.env.DB_ENCRYPT,
  minConn: process.env.DB_MIN_CONN || 0,
  maxConn: process.env.DB_MAX_CONN || 5,
  idleTimeout: process.env.DB_IDLE_TIMEOUT || 30000,
  acquireTimeout: process.env.DB_ACQUIRE_TIMEOUT || 10000,
  timeout: process.env.DB_TIMEOUT || 5000,
};

module.exports = {
  app,
  logs,
  database,
};