const express = require("express");
const logger = require("../../lib/logs");
const httpErrorResponse = require("../../lib/http_error_response");
const Task = require("../../models/task");

function routes() {
  const router = express.Router();

  router.get("/",
    async function (req, res) {
      try {
        Task.getTasks()
          .then(tasks => {
            res.send(tasks);
          })
          .catch(err => {
            logger.error(err);
            httpErrorResponse(res, 500, "Failed to retrieve tasks.", err);
          });
      } catch (e) {
        logger.error(e);
        httpErrorResponse(res, 500, "Sorry, something went wrong.", e.message);
      }
    });

  router.post("/",
    async function (req, res) {
      try {
        const params = {
          name: req.body.name,
          details: req.body.details,
          priority_id: req.body.priority_id,
        };
        if (!params.name) {
          httpErrorResponse(res, 400, "Missing task name.", params);
          return;
        }

        Task.createTask(params)
          .then(taskId => {
            Task.getTask(taskId)
              .then(task => {
                res.status(201).send(task);
              });
          })
          .catch(err => {
            logger.error(err);
            httpErrorResponse(res, 500, "Failed to create task.", err);
          });
      } catch (e) {
        logger.error(e);
        httpErrorResponse(res, 500, "Sorry, something went wrong.", e.message);
      }
    });

  router.delete("/:id",
    async function (req, res) {
      try {
        Task.deleteTask(req.params.id)
          .then(() => {
            res.status(204).send();
          })
          .catch(err => {
            logger.error(err);
            httpErrorResponse(res, 500, "Failed to delete task.", err);
          });
      } catch (e) {
        logger.error(e);
        httpErrorResponse(res, 500, "Sorry, something went wrong.", e.message);
      }
    });

  logger.info("Task routes loaded.");

  return router;
}

module.exports = routes;