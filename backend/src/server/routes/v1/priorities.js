const express = require("express");
const logger = require("../../lib/logs");
const httpErrorResponse = require("../../lib/http_error_response");
const {request} = require("../../lib/database");
const HttpHelper = require("../../lib/helper");

function routes() {
  const router = express.Router();

  router.get("/",
    async function (req, res) {
      try {
        const sqlQuery = "SELECT * FROM [dbo].[priorities]";
        const dbRequest = await request();
        dbRequest.query(sqlQuery)
          .then(result => {
            const priorities = HttpHelper.processDBResults(result);
            res.send(priorities);
          })
          .catch(err => {
            logger.error(err);
            httpErrorResponse(res, 500, "Failed to retrieve priorities.", err);
          });
      } catch (e) {
        logger.error(e);
        httpErrorResponse(res, 500, "Sorry, something went wrong.", e.message);
      }
    });

  logger.info("Priority routes loaded.");

  return router;
}

module.exports = routes;