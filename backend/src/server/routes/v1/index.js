const express = require("express");
const taskRoutes = require("./tasks");
const priorityRoutes = require("./priorities");
const logger = require("../../lib/logs");

function v1Routes() {

  const router = express.Router();

  router.use("/priorities", priorityRoutes());
  router.use("/tasks", taskRoutes());

  logger.info("V1 routes loaded.");

  return router;
}

module.exports = v1Routes;