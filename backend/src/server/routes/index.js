const express = require("express");
const v1Routes = require("./v1");
const logger = require("../lib/logs");

class Root {
  constructor() {
    this.init();
    this.middleware();
    this.routes();
  }

  init() {
    this.router = express.Router();
  }

  middleware() {
  }

  routes() {
    this.router.use("/api/v1", v1Routes());
    this.router.use("/api", v1Routes());
    logger.info("All routes loaded.");
  }
}

module.exports = Root;