const {request, sql} = require("../lib/database");
const Helper = require("../lib/helper");

class Task {
  static async getTasks() {
    const sqlQuery = "SELECT tasks.*, priorities.name as priority_name " +
      "FROM [dbo].[tasks] as tasks " +
      "LEFT JOIN [dbo].[priorities] as priorities ON tasks.priority_id = priorities.id";
    const dbRequest = await request();
    return dbRequest.query(sqlQuery)
      .then(result => {
        return Helper.processDBResults(result);
      });
  }

  static async getTask(id) {
    const sqlQuery = "SELECT tasks.*, priorities.name as priority_name " +
      "FROM [dbo].[tasks] as tasks " +
      "LEFT JOIN [dbo].[priorities] as priorities ON tasks.priority_id = priorities.id " +
      "WHERE tasks.id = @id";
    const dbRequest = await request();
    return dbRequest.input("id", sql.BigInt, id)
      .query(sqlQuery)
      .then(result => {
        return Helper.processDBSingleResult(result);
      });
  }

  static async createTask(task) {
    const sqlQuery =
      "INSERT INTO [dbo].[tasks] ([name], [details], [priority_id]) VALUES (@name, @details, @priority_id); " +
      "SELECT @@IDENTITY";
    const dbRequest = await request();
    return dbRequest
      .input("name", sql.VarChar, task.name)
      .input("details", sql.VarChar, task.details)
      .input("priority_id", sql.BigInt, task.priority_id)
      .query(sqlQuery)
      .then(result => {
        return Helper.processDBSingleResult(result)[""];
      });
  }

  static async deleteTask(id) {
    const sqlQuery = "DELETE [dbo].[tasks] WHERE [id] = @id";
    const dbRequest = await request();
    return dbRequest.input("id", sql.BigInt, id)
      .query(sqlQuery);
  }
}

module.exports = Task;