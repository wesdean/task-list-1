const http = require("http");
const express = require("express");
const Root = require("./routes");
const logger = require("./lib/logs");
const bodyParser = require("body-parser");
const cors = require("cors");

class App {
  constructor() {
    this.init();
    this.middleware();
    this.routes();
  }

  init() {
    this.app = express();
    this.server = http.createServer(this.app);
  }

  middleware() {
    this.app.use(cors());
    this.app.use(bodyParser.urlencoded({extended: false}));
    this.app.use(bodyParser.json());
  }

  routes() {
    logger.info("Loading routes for app.");
    this.app.use("/", (new Root()).router);
  }

  shutdown() {
    this.server.close();
  }
}

module.exports = App;