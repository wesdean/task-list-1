function httpErrorResponse(response, httpStatus, errorMessage, debugMessage) {
  response.status(httpStatus).send({error: errorMessage, debug: debugMessage});
}

module.exports = httpErrorResponse;