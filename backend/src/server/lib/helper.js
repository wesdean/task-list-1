class Helper {
  static processDBResults(results) {
    if (results && results.recordset && Array.isArray(results.recordset)) {
      return results.recordset;
    }
  }

  static processDBSingleResult(results) {
    if (results && results.recordset && Array.isArray(results.recordset) && results.recordset.length === 1) {
      return results.recordset[0];
    }
  }
}

module.exports = Helper;