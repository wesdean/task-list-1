const {logs} = require("../config");
const log4js = require('log4js');

log4js.configure(logs);
const logger = log4js.getLogger();
logger.debug("Logger started");

module.exports = logger;