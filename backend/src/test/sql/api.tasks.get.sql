use [tasklist];
truncate table [dbo].[tasks];

INSERT INTO [dbo].[tasks] ([name], [details], [priority_id])
VALUES ('Test 1', 'Test 1 details', 1);

INSERT INTO [dbo].[tasks] ([name], [details], [priority_id])
VALUES ('Test 2', 'Test 2 details', 2);

INSERT INTO [dbo].[tasks] ([name], [details], [priority_id])
VALUES ('Test 3', 'Test 3 details', 3);