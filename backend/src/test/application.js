const App = require("../server/app");

let app;

module.exports = {
  instance: function () {
    if (!app) {
      app = new App();
    }
    return app;
  },
  shutdown: function () {
    if (app) {
      setTimeout(() => {
        app.shutdown();
        app = null;
      });
    }
  }
};