require("../hooks");
const application = require("../application");
const chai = require("chai");
const chaiHttp = require("chai-http");
const expect = require("chai").expect;
const Database = require("../database");
const Task = require("../../server/models/task");

const database = new Database();

chai.use(chaiHttp);

describe("GET /tasks", () => {
  let app = null;

  before(async () => {
    await database.resetDatabase(`${__dirname}/../sql/api.tasks.get.sql`);
    app = application.instance();
  });

  it("should return a list of tasks", done => {
    chai.request(app.server)
      .get("/api/tasks")
      .send()
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(Array.isArray(res.body)).to.be.true;
        expect(res.body.length).to.equal(3);
        expect(res.body[1].name).to.equal("Test 2");
        done();
      })
  });
});

describe("POST /tasks", () => {
  let app = null;

  before(async () => {
    database.resetDatabase(`${__dirname}/../sql/api.tasks.create.sql`);
    app = application.instance();
  });

  it("should create a task and return the new task", done => {
    chai.request(app.server)
      .post("/api/tasks")
      .send({
        name: "New Task"
      })
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(201);
        expect(res).to.be.json;
        expect(res.body).to.be.a("object");
        expect(res.body.name).to.equal("New Task");
        done();
      })
  });
});

describe("DELETE /tasks", () => {
  let app = null;

  before(async () => {
    database.resetDatabase(`${__dirname}/../sql/api.tasks.get.sql`);
    app = application.instance();
  });

  it("should delete a task", done => {
    let taskId = 2;
    chai.request(app.server)
      .delete(`/api/tasks/${taskId}`)
      .send()
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(204);

        Task.getTask(taskId)
          .then(task => {
            expect(task).to.be.a("undefined");
            done();
          })
          .catch(err => {
            expect(JSON.stringify(err)).to.be.null;
            done();
          });
      })
  });
});