require("../hooks");
const application = require("../application");
const chai = require("chai");
const chaiHttp = require("chai-http");
const expect = require("chai").expect;

chai.use(chaiHttp);

describe("GET /priorities", () => {
  let app = null;

  before(async () => {
    app = application.instance();
  });

  it("should return a list of priorities", done => {
    chai.request(app.server)
      .get("/api/priorities")
      .send()
      .end((err, res) => {
        expect(err).to.be.null;
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(Array.isArray(res.body)).to.be.true;
        expect(res.body.length).to.equal(3);
        done();
      })
  });
});