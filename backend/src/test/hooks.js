const application = require("./application");

before("global before", () => {
  application.instance();
});

after("global after", () => {
  setTimeout(() => application.shutdown());
});