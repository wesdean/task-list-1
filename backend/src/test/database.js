const sql = require("mssql");
const {database} = require("../server/config");
const fs = require("fs");

const config = {
  server: database.hostname,
  user: "tasklist-dbo",
  password: process.env.DB_PASSWORD,
  database: database.databaseName,
  encrypt: database.encrypt,
  pool: {
    min: database.minConn,
    max: database.maxConn,
    idleTimeoutMillis: database.idleTimeout
  },
  requestTimeout: database.timeout,
  acquireConnectionTimeout: database.acquireTimeout,
};

let pool = null;

const db = {
  async pool(refresh = false) {

    if (refresh || !pool) {
      try {
        const connections = new sql.ConnectionPool(config);
        pool = await connections.connect();
      } catch (err) {
        console.error(err);
      }
    }

    return pool;

  },
  async transaction(scope = async (trx) => {
  }) {

    return db.pool()
      .then(conn => new sql.Transaction(conn))
      .then(trx =>

        scope(trx)
          .then(result =>

            trx.commit()
              .then(() => result)
          )
          .catch(err => {

            trx.rollback();
            return Promise.reject(err);

          })
      )
      ;

  },
  async request(connection) {

    const conn = (connection || await db.pool());
    return new sql.Request(conn);

  },
  close() {

    if (pool) {
      pool.close();
      pool = null;
    }

  }
};

class Database {
  constructor() {
    this.request = db.request;
  }

  async resetDatabase(sqlFile) {
    if (sqlFile && fs.existsSync(sqlFile)) {
      const sqlQuery = fs.readFileSync(sqlFile).toString();
      if (sqlQuery) {
        const dbRequest = await this.request();
        return dbRequest.query(sqlQuery);
      }
    }
  }
}

module.exports = Database;